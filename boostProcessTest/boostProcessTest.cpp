#pragma once

#include "Process.h"

#include<functional>
#include<iostream>
#include<string>

#include<boost/asio/spawn.hpp>
#include<boost/asio.hpp>


::boost::asio::io_context io;

void simulator(::boost::asio::yield_context _yield)
{
	while (true)
	{
		std::string cmd, input, out;
		
		std::cout << "command: ";
		std::cin >> cmd;
        
		if (!cmd.compare("exit")) break;

		std::cout << "input: ";
		std::getchar();
		std::getline(std::cin, input);
	
		Process child(io);
	
		std::error_code e = child.create_async("sleep 120", input, out, _yield);
		
		std::cout << e.message();
		std::cout << out << std::endl;
	}
}

int main()
{
	
	::boost::asio::spawn(io, std::bind(simulator, std::placeholders::_1));
	io.run();

    return 0;
}

