#pragma once

#define _SCL_SECURE_NO_WARNINGS

#define BOOST_COROUTINES_NO_DEPRECATION_WARNING
#define _WIN32_WINNT 0x0501

#include <functional>
#include <string>
#include <vector>
#include <chrono>

#include <boost/process.hpp>

#include <boost/asio/steady_timer.hpp>
#include <boost/asio/spawn.hpp>
#include <boost/asio.hpp>

namespace bp = ::boost::process;

class Process
{
public:

	using yield_context = ::boost::asio::yield_context;
	using steady_timer = ::boost::asio::steady_timer;
	using io_context = ::boost::asio::io_context;

	Process(io_context &io);
	~Process();

	//void create_async(const std::string& _cmd, const std::string& _input, std::string& _output, yield_context _yield);
	std::error_code create_async(const std::string& _cmd, const std::string &_input, std::string &_output, yield_context& _yield);

private:

	void doTimeout(yield_context _yield);

	std::shared_ptr<bp::child> m_child;
	std::error_code m_ret;

	steady_timer m_timer;
	bp::async_pipe m_pipe;

	unsigned int m_timeout = 10;
};