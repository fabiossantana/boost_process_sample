#pragma once

#define _WIN32_NNT 0x0501

#include<boost/asio/steady_timer.hpp>
#include<boost/asio.hpp>

#include<iostream>
#include<utility>
#include<memory>
#include<chrono>

using steady_timer = ::boost::asio::steady_timer;
using io_context = ::boost::asio::io_context;

using  boost::asio::ip::tcp;
using namespace std;

unsigned int connections = 0;

class session : public enable_shared_from_this<session>
{
public:
	session(tcp::socket socket) : m_socket(move(socket)), m_timer(m_socket.get_io_context())
	{
		m_timer.expires_at(steady_timer::time_point::max());
	}

	void start()
	{
		connections++;
		if (connections == 1)
		{
			m_timer.expires_after(chrono::seconds(30));
			m_timer.async_wait(bind(&session::timeout, shared_from_this(), &m_timer));
		}
		else
		{
			do_read();
		}
	}

private:

	void do_read()
	{
		auto self(shared_from_this());
		m_socket.async_read_some(boost::asio::buffer(data_, max_length),
			[this, self](boost::system::error_code ec, std::size_t length)
		{
			if (!ec)
			{
				do_write(length);
			}
		});
	}

	void do_write(std::size_t length)
	{
		auto self(shared_from_this());
		boost::asio::async_write(m_socket, boost::asio::buffer(data_, length),
			[this, self](boost::system::error_code ec, std::size_t /*length*/)
		{
			if (!ec)
			{
				do_read();
			}
		});
	}

	void timeout(steady_timer *timer)
	{
		if (timer->expiry() <= steady_timer::clock_type::now())
		{
			if (m_socket.is_open())
			{
				cout << "close socket" << std::endl;
				::boost::system::error_code ec;
				m_socket.close(ec);
			}
		}
		else
		{
			timer->async_wait(bind(&session::timeout, shared_from_this(), timer));
		}
	}

	tcp::socket m_socket;
	steady_timer m_timer;

	enum { max_length = 1024 };
	char data_[max_length];
};

class server
{
public:
	server(boost::asio::io_context& io_context, short port)
		: acceptor_(io_context, tcp::endpoint(tcp::v4(), port))
	{
		acceptor_.set_option(tcp::acceptor::reuse_address(true));
		do_accept();
	}

private:
	void do_accept()
	{
		acceptor_.async_accept(
			[this](boost::system::error_code ec, tcp::socket socket)
		{
			if (!ec)
			{
				std::make_shared<session>(std::move(socket))->start();
			}

			do_accept();
		});
	}

	tcp::acceptor acceptor_;
};

int main(int argc, char* argv[])
{
	try
	{
		boost::asio::io_context io_context;

		server s(io_context, 4000);

		io_context.run();
	}
	catch (std::exception& e)
	{
		std::cerr << "Exception: " << e.what() << "\n";
	}

	return 0;
}
